## Registro de Actividades


** 15/05/2016 **  

- Bajo una nueva copia del master del firmware, para tener como referencia del sistema de generate y build.
- Se estudia el sistema de build del firmware.

** 18/05/2016 **  

- Se estudia el ABI de la arquitectura SPARC. 
- Se estudia el mecanismo de ventanas de la arquitectura SPARC, y su relación con la pila de los hilos de ejecución.
- Se estudia el set de instrucciones disponibles en el procesador.

** 19/05/2016 **  

- Se analizan ejemplos del código de cambio de contexto de otros sistemas operativos de tipo open source (RTEMS, FreeRTOS). 

** 19/06/2016 **  

- Se estudia el funcionamiento del port del firmware CIAA para la arquitectura Cortex M4, para determinar el funcionamiento de las interaces entre el OSEK y las rutinas de administración de contextos, las pilas de tareas, inicialización de tareas, y qué nivel de participación tiene el generador en PHP en la generación de estas estructuras de código. 
- Se estudia el funcionamiento de la rutina schedule(), fundamental para comprender el sistema de administración de tareas de OSEK. 
- Se examinan los diversos contextos de ejecución donde puede ejecutarse el código de la capa de compatibilidad: aplicación, sistema operativo e interrupción.

** 20/06/2016 **  

- A la luz del conocimiento del port de la arquitectura Cortex M4 del firmware, se estudia de forma comparativa el port para X86, que por las características de la propia arquitectura presenta una mejor cohesión a la interfaz del OSEK a las capas de compatibilidad. Se estudia las diferencias en términos de estructuras utilizadas para almacenar contextos, administración de las pilas, inicialización de tareas, e interrupciones. 

** 27/06/2016 **  

- Se repasa el ABI de la arquitectura SPARC, para estudiar la conformación de los frames en la pila del sistema y los elementos que es necesario considerar como parte del contexto de una tarea.

** 29/06/2016 **  

- Se pone al día este registro de actividades.
- Se repasa el set de instrucciones de la arquitectura SPARC. 

** 30/06/2016 **  

- Se investiga como almacenar el estado de los registros globales durante la ejecución anidada de interrupciones, y qué relacion tiene esto con el cambio de contexto de tareas realizado desde rutinas de interrupción.

** 16/07/2016 **  

- Se imprime el material de mayor relevancia para utilizar durante la codificación, en copia en papel: resumen de set de instrucciones, ABI del procesador, conjunto de registros, esquema de utilización de pila, y el capítulo "Software considerations" del SPARC v8 Manual.
- Se estudian ejemplos de rutinas de administración de la pila y los registros de ventanas, obtenidas de los códigos fuentes de RTEMS, FreeRTOS y del BCC de Gaisler.

** 17/07/2016 **  
Se define que la mejor aproximación para realizar el cambio de contexto es implementar las rutinas que cambian el contexto de ejecución como interrupciones de software del sistema, e implementar el salvado del contexto, su recuperación como parte de la cáscara de administración de interrupciones del sistema.

Básicamente, al atenderse una interrupción/trap por software,

- el controlador determina si la interrupción es la más externa o si está anidada dentro de una interrupción previa.
- Si la interrupción se encuentra anidada
    - Se garantiza que exista al menos una ventana libre para una eventual interrupción/trap posterior
    - Se guarda el contexto de la interrupción anterior en la pila.
    - Se ejecuta la rutina de servicio de interrupción del usuario.
    - Se recupera el contexto de la interrupción interrumpida.
    - Se finaliza la ejecución de la interrupción anidada.
- Si la interrupción es la más externa:
    - Se vuelcan todas las ventanas excepto la trap window a la pila.
    - Se guarda el resto del contexto de la tarea interrumpida en la pila, incluyendo el contexto de punto flotante (si fuera necesario).
    - Se habilitan las interrupciones.
    - Se ejecuta la rutina de servicio de interrupción del usuario. Esta rutina puede provocar un cambio de la tarea activa.
    - Se deshabilitan las interrupciones.
    - Se recupera el contexto de la tarea activa almacenada en la pila.
    - Se finaliza la ejecución de la interrupción, volviendo la ejecución a la tarea anidada.

** 25/07/2016 **  

- Se escribe el código de las rutinas correspondientes al tratamiento de los traps de overflow y underflow.

** 27/07/2016 **  

- Se escribe el código del envoltorio de las interrupciones/traps genérias, responsable de almacenar el contexto de las tareas interrumpidas, y recuperar el contexto de las tareas activas luego de la ejecución del código de tratamiento de interrupción de usuario.
- Se completó el caso de interrupciones en caso externo, quedando pendiente el tratamiento del caso de la rutina de interrupción anidada.

** 29/07/2016 **  

- Se completa el caso de tratamiento de la rutina de interrupción anidada. 
- Se repasó el código buscando errores en la lógica o en la implementación.
- Se pasaron en limpio los comentarios explicativos del código.
- Se ingresó al control de versiones el código generado, para garantizar su conservación.

** 01/08/2016 **  

- Se actualiza este registro de actividades con las actividades realizadas en Julio.


** 01/08/2016 **  

- Se actualiza este registro de actividades con las actividades realizadas en Julio.


** 04/08/2016 **  

Se escribió la rutina de excepción del syscall del sistema, que realiza determinadas tareas que deben ser realizadas con las interrupciones desactivadas (habilitar y deshabilitar interrupciones, deshabilitar traps, reiniciar el sistema, etc.).

** 07/08/2016 **  

Se extendió la rutina del trap de syscal y se modificó el universalhandler para que el código de retorno funciona tanto para traps por software como traps de interrupción.

** 08/08/2016 **  

- Se definieron una serie de funciones para acceder a los syscalls del sistema sin llamar directamente a la instrucción trap.

** 09/08/2016 **  

- Se mejoró el código de retorno de la rutina de universalhandler para reejecutar la instrucción donde se produce el trap sólo para los interrupting traps (el código anterior era un poco más genérico en cuanto cómo resolvía eso).
- Se agregó una rutina de syscall nueva, para llamar al depurador del sistema. 
- Se creó un trap handler para todos los vectores que no tengan un handler definido.
- Se creó la primera versión de la tabla de traps  del sistema.

** 12/08/2016 **  

- Se completó el retrabajo de los archivos Os_Arch.c y Os_Internal_Arch.c, eliminando código específico del por de Cortex, y adaptando el contenido para el port de Leon. 
- Se mejoró también el estilo, reordenando el código a las secciones que tiene el esquema oficial del firmware, el cual no era totalmente respetado por los archivos originales del port de Cortex. Mejoró bastante la comprensibilidad de del código. 
- Se analizaron porciones del código, y se llegó a la conclusión de que algunas de las partes más ocuras del código base son en realidad consecuencia de las características del port Cortex, y no son necesarias para el port Leon, simplificando notablemente el código.
- Se escribió el código que inicializa la pila de las tareas y establece el punto de arranque de las tareas, que es un punto crítico del funcionamiento del port. 

















----

Se puede encontrar un resumen de formato utilizando Markdown en  

https://github.com/gitlabhq/gitlabhq/blob/master/doc/markdown/markdown.md

