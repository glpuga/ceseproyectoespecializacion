## Reporte de problema PR-0002

**Resumen:**  
Uso de Gerrit en el desarrollo de la CIAA

**Responsables:**:  

- Gerardo Puga

**Estado actual:**  

CERRADO

** Descripción:**  

Determinar si va a ser necesario utilizar Gerrit para interactuar con el proyecto CIAA en lugar de hacerlo a través del repositorio en Github de forma directa.

## Estado de avance

** 17/04/2016 **  
Creación del reporte.

---

** 17/04/2016 **  

Rastrillando el wiki y la lista de correo  se encontró la siguiente relativa al Gerrit:

Guía de uso:
http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:repositorio

Declaración de que se utiliza Gerrit como parte de los procesos del desarrollo:
http://proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:proceso

Thread de la lista donde Mariano habla de Gerrit y su relación con GitHub:
https://groups.google.com/forum/#!msg/ciaa-firmware/wp2ePEWkDTI/_Z5eBR1OFgAJ

---

** 17/04/2016 **  

El paso siguiente probablemente es realizar una consulta sobre Gerrit en la lista de correo. 

---

** 07/05/2016 **  

Consultando a la lista de correo **Mariano descartó la idea de trabajar a partir del respositorio en GerritHub**, ya que dice que la colaboración a través de este último nunca fue implementada completamente. 

---

** 08/05/2016 **  

**Se cierra el reporte de problema ya que según la información dada por Mariano, GerritHub va a ser irrelevante para el resto del desarrollo del proyecto.**


