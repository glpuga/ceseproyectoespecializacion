**Desarrollo de la capa de compatibilidad del firmware  Edu-CIAA para el hardware Edu-CIAA Xilinx**

**Índice de reportes de problemas**


| CODIGO       | ASUNTO                                        | RESPONSABLES       | ESTADO        |
| --------------: | :-------------------------------------------- | :----------------- | :-----------: | 
| PR-0001       | Actividad 2.2 Recopilar y estudiar las  convenciones utilizadas por la comunidad  | Gerardo Puga       | CERRADO |
| PR-0002       | Uso de Gerrit en el desarrollo de la CIAA                                                                   | Gerardo Puga       | CERRADO |



