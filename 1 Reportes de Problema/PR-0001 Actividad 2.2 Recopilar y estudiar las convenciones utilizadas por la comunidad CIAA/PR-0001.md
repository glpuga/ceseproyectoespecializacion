## Reporte de problema PR-0001

**Resumen:**  
Actividad 2.2 Recopilar y estudiar las convenciones utilizadas por la comunidad CIAA

**Responsables:**:  

- Gerardo Puga

**Estado actual:**  

CERRADO

** Descripción:**  

Determinar que convenciones de trabajo y estilo se utilizan en la comunidad de desarrollo del firmware CIAA: estilos de programación, workflow de trabajo en el repositorio central, etc.

## Estado de avance

** 17/04/2016 **  
Creación del reporte.

---

** 17/042016 **  

Recorriendo el wiki del proyecto se encontró una resumen de las normas de estilo en la página:

http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:fw_coding_guidelines

que puede resumirse en:

1. "3 espacios y no tabs."
2. "*No trailing spaces.*"
3. "Los comentarios del código deberán estar escritos en inglés."
4. "Los comentarios deberán tener el mismo nivel de sangrado que el código que comentan."
5. "Los comentarios no deberán repetir y/o reformular el código, ya que su propósito es explicar la intención del código."
6. "No se recomiendan los comentarios de fin de línea (doble barra inclinada //)."
7. "Los comentarios de funciones, macros, types, etc. deberán estar hechos siguiendo la sintaxis utilizada por el generador automático de documentación Doxygen."
8. "La estructura de cada nuevo archivo debe estar basada en los templates definidos para tal fin."
9. Macros: 
		a. "*POSIX or standard macros use the name as defined in the standard with the ciaaPOSIX_prefix.*"
		b. "*Non standard interface use the following schema ModuleName_MACRO_NAME.*"
10. Nombre de funciones:
		a. "*POSIX or standard interfaces use the name as defined in the standard with the ciaaPOSIX_ prefix.*"
		b. "*Non standard interfaces use following schema ModuleName_functionName.*"

Respecto de la utiización del repositorio, demanda la utilización de mensajes descriptivos de cada commit (no dejar el mensaje en blanco). 

En el caso de "actualizaciones relacionadas con el firmware", demanda también incluir en el mensaje el número de ticket del sistema de *__issues__* de GitHub, como en el siguiente ejemplo:

>git commit -m ”#123 Descripción del commit vinculado al ticket 123 y también al #246 por tal y cual cosa!”

---

** 17/042016 **  
Se encontró información algo fuera de época de la estructura de directorios que tiene el firmware. 

http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:estructura_de_directorios

---

** 17/042016 **  
Aparece nombrada otra herramienta en una de las páginas del wiki, una llamda Uncrustify, que pacere tener por misión verificar el seguimiento de cierto estilo de escritura de código.

No queda claro que su utilización no sea simplemente para realizar un chequeo previo al cierre de un release, porque se supone que el estilo es algo que los programadores respetan durante la codificación, no algo que están corrigiendo todo el tiempo mediante este software.

---

** 17/042016 **  
Otra herramienta mencionada es GerritHub, que se usa para hacer un seguimiento de las revisiones de los aportes de código al master del firmware. 

Creo un reporte de problema para investigar eso, porque aparentemente acutalmente todos los aportes al código se hacen a través de esa herramienta. Reporte creado RP-0002.

Algo de información al respecto en la wiki de la CIAA se puede encontrar aquí:

http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:repositorio
 
y acá

http://proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:proceso

y una discusión donde Mariano habla de Gerrit en aquí:

https://groups.google.com/forum/#!msg/ciaa-firmware/wp2ePEWkDTI/_Z5eBR1OFgAJ


Cómo operar con GitHub y Gerrit, según GerritHub,
https://review.gerrithub.io/static/intro.html

---

** 17/042016 **  

Criterio de nombramiento de branches:

http://proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:proceso:criterio_branches

Los lineamientos se resumen en las siguientes pautas:

* Para desarrollar nuevas funcionalides: **feature/**nombreDeNuevaFunc
* Para nuevos releases: **feature/**nombreDeNuevaFunc
* Para mejoras de código: **improvement/**nombreDeNuevaFunc
* Para nuevas herramientas: **tool/**nombreDeNuevaFunc

---

** 17/042016 **  

Criterio de releases:
http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:firmware:plan_desarrollo_fw

Se sumariza en:

* Tres releases al año.
* Dos de ellos son STS (abril y agosto), y el tercero de una versión LTS (diciembre).


---

** 07/05/2016 **  

A partir de una consulta a la lista de correo, responde Mariano que lo mejor para llevar adelante el trabajo es llevar un branch en el repositorio público, bajo la denominación "features/xxxxxx", de forma de llevar el trabajo de forma pública para otros desarrolladores. 

Recomendó en contra de usar un fork, porque de esa forma los demás desarrolladores no vas a estar mirando los cambios que yo haga. 

Recomendó trabajar a partir del último release, pero en vista de que cree que no hubieron cambios en el master que sean relevantes parami trabajo desde el release de diciembre, dice que no hay problemas con trabajar a partir del master.

Descartó la utilización de Gerrit, ya que dice que finalmente nunca fue implementado. Debiera partir mi trabajo del repositorio en GitHub.

---

** 08/05/2016 **  

Se cierra el reporte de problema ya que se considera que se ha recopilado toda la información relevante respecto de las normas de trabajo y estilo para desarrollar el firmware CIAA. 

